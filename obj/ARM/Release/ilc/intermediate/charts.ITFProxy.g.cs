using Mcg.System;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.WindowsRuntime;



namespace __InterfaceProxies
{
	public class ServiceChannelProxy_IReportService : global::System.ServiceModel.Channels.ServiceChannelProxy_P, global::Syncfusion.Reports.Server.IReportService_P
	{
		global::System.Threading.Tasks.Task_P<byte[]> global::Syncfusion.Reports.Server.IReportService_P.GetReportAsync(global::Syncfusion.Reports.Server.ReportSetting_P setting)
		{
			global::System.RuntimeMethodHandle interfaceMethod = global::System.Reflection.DispatchProxyHelpers.GetCorrespondingInterfaceMethodFromMethodImpl();
			global::System.RuntimeTypeHandle interfaceType = typeof(global::Syncfusion.Reports.Server.IReportService_P).TypeHandle;
			global::System.Reflection.MethodBase targetMethodInfo = global::System.Reflection.MethodBase.GetMethodFromHandle(
								interfaceMethod, 
								interfaceType
							);
			object[] callsiteArgs = new object[] {
					setting
			};
			global::System.Threading.Tasks.Task_P<byte[]> retval = ((global::System.Threading.Tasks.Task_P<byte[]>)base.Invoke(
								((global::System.Reflection.MethodInfo)targetMethodInfo), 
								callsiteArgs
							));
			return retval;
		}

		global::System.Threading.Tasks.Task_P<global::Syncfusion.Reports.Server.SharedDatasetinfo_P> global::Syncfusion.Reports.Server.IReportService_P.GetSharedDataSetAsync(
					global::Syncfusion.Reports.Server.ReportSetting_P setting, 
					string path)
		{
			global::System.RuntimeMethodHandle interfaceMethod = global::System.Reflection.DispatchProxyHelpers.GetCorrespondingInterfaceMethodFromMethodImpl();
			global::System.RuntimeTypeHandle interfaceType = typeof(global::Syncfusion.Reports.Server.IReportService_P).TypeHandle;
			global::System.Reflection.MethodBase targetMethodInfo = global::System.Reflection.MethodBase.GetMethodFromHandle(
								interfaceMethod, 
								interfaceType
							);
			object[] callsiteArgs = new object[] {
					setting, 
					path
			};
			global::System.Threading.Tasks.Task_P<global::Syncfusion.Reports.Server.SharedDatasetinfo_P> retval = ((global::System.Threading.Tasks.Task_P<global::Syncfusion.Reports.Server.SharedDatasetinfo_P>)base.Invoke(
								((global::System.Reflection.MethodInfo)targetMethodInfo), 
								callsiteArgs
							));
			return retval;
		}

		global::System.Threading.Tasks.Task_P<global::Syncfusion.Reports.Server.RecordInfo_P> global::Syncfusion.Reports.Server.IReportService_P.GetDataAsync(
					global::Syncfusion.Reports.Server.ReportSetting_P Setting, 
					global::Syncfusion.Reports.Server.ReportDataInfo_P ReportInfo)
		{
			global::System.RuntimeMethodHandle interfaceMethod = global::System.Reflection.DispatchProxyHelpers.GetCorrespondingInterfaceMethodFromMethodImpl();
			global::System.RuntimeTypeHandle interfaceType = typeof(global::Syncfusion.Reports.Server.IReportService_P).TypeHandle;
			global::System.Reflection.MethodBase targetMethodInfo = global::System.Reflection.MethodBase.GetMethodFromHandle(
								interfaceMethod, 
								interfaceType
							);
			object[] callsiteArgs = new object[] {
					Setting, 
					ReportInfo
			};
			global::System.Threading.Tasks.Task_P<global::Syncfusion.Reports.Server.RecordInfo_P> retval = ((global::System.Threading.Tasks.Task_P<global::Syncfusion.Reports.Server.RecordInfo_P>)base.Invoke(
								((global::System.Reflection.MethodInfo)targetMethodInfo), 
								callsiteArgs
							));
			return retval;
		}

		global::System.Threading.Tasks.Task_P<global::Syncfusion.Reports.Server.ServiceDataSourceDefinition_P> global::Syncfusion.Reports.Server.IReportService_P.GetSharedDataSourceDefinitionAsync(
					global::Syncfusion.Reports.Server.ReportSetting_P setting, 
					string dataSource)
		{
			global::System.RuntimeMethodHandle interfaceMethod = global::System.Reflection.DispatchProxyHelpers.GetCorrespondingInterfaceMethodFromMethodImpl();
			global::System.RuntimeTypeHandle interfaceType = typeof(global::Syncfusion.Reports.Server.IReportService_P).TypeHandle;
			global::System.Reflection.MethodBase targetMethodInfo = global::System.Reflection.MethodBase.GetMethodFromHandle(
								interfaceMethod, 
								interfaceType
							);
			object[] callsiteArgs = new object[] {
					setting, 
					dataSource
			};
			global::System.Threading.Tasks.Task_P<global::Syncfusion.Reports.Server.ServiceDataSourceDefinition_P> retval = ((global::System.Threading.Tasks.Task_P<global::Syncfusion.Reports.Server.ServiceDataSourceDefinition_P>)base.Invoke(
								((global::System.Reflection.MethodInfo)targetMethodInfo), 
								callsiteArgs
							));
			return retval;
		}

		global::System.Threading.Tasks.Task_P<global::Syncfusion.Reports.Server.ExportData_P> global::Syncfusion.Reports.Server.IReportService_P.ExportAsync(
					global::Syncfusion.Reports.Server.ReportSetting_P setting, 
					string exportType)
		{
			global::System.RuntimeMethodHandle interfaceMethod = global::System.Reflection.DispatchProxyHelpers.GetCorrespondingInterfaceMethodFromMethodImpl();
			global::System.RuntimeTypeHandle interfaceType = typeof(global::Syncfusion.Reports.Server.IReportService_P).TypeHandle;
			global::System.Reflection.MethodBase targetMethodInfo = global::System.Reflection.MethodBase.GetMethodFromHandle(
								interfaceMethod, 
								interfaceType
							);
			object[] callsiteArgs = new object[] {
					setting, 
					exportType
			};
			global::System.Threading.Tasks.Task_P<global::Syncfusion.Reports.Server.ExportData_P> retval = ((global::System.Threading.Tasks.Task_P<global::Syncfusion.Reports.Server.ExportData_P>)base.Invoke(
								((global::System.Reflection.MethodInfo)targetMethodInfo), 
								callsiteArgs
							));
			return retval;
		}

		global::System.Threading.Tasks.Task_P<bool> global::Syncfusion.Reports.Server.IReportService_P.IsValidConnectionAsync(
					string connectionString, 
					string dataProvider)
		{
			global::System.RuntimeMethodHandle interfaceMethod = global::System.Reflection.DispatchProxyHelpers.GetCorrespondingInterfaceMethodFromMethodImpl();
			global::System.RuntimeTypeHandle interfaceType = typeof(global::Syncfusion.Reports.Server.IReportService_P).TypeHandle;
			global::System.Reflection.MethodBase targetMethodInfo = global::System.Reflection.MethodBase.GetMethodFromHandle(
								interfaceMethod, 
								interfaceType
							);
			object[] callsiteArgs = new object[] {
					connectionString, 
					dataProvider
			};
			global::System.Threading.Tasks.Task_P<bool> retval = ((global::System.Threading.Tasks.Task_P<bool>)base.Invoke(
								((global::System.Reflection.MethodInfo)targetMethodInfo), 
								callsiteArgs
							));
			return retval;
		}
	}

	[global::System.Runtime.CompilerServices.EagerStaticClassConstruction]
	[global::System.Runtime.CompilerServices.DependencyReductionRoot]
	public static class __DispatchProxyImplementationData
	{
		static global::System.Reflection.DispatchProxyEntry[] s_entryTable = new global::System.Reflection.DispatchProxyEntry[] {
				new global::System.Reflection.DispatchProxyEntry() {
					ProxyClassType = typeof(global::System.ServiceModel.Channels.ServiceChannelProxy_P).TypeHandle,
					InterfaceType = typeof(global::Syncfusion.Reports.Server.IReportService_P).TypeHandle,
					ImplementationClassType = typeof(global::__InterfaceProxies.ServiceChannelProxy_IReportService).TypeHandle,
				}
		};
		static __DispatchProxyImplementationData()
		{
			global::System.Reflection.DispatchProxyHelpers.RegisterImplementations(s_entryTable);
		}
	}
}

namespace Syncfusion.Reports.Server
{
	[global::System.Runtime.InteropServices.McgRedirectedType("Syncfusion.Reports.Server.IReportService, Syncfusion.SfReportViewer.UWP, Version=14.1460.0.41, Culture=neutral, " +
		"PublicKeyToken=3d67ed1f87d44c89")]
	public interface IReportService_P
	{
		global::System.Threading.Tasks.Task_P<byte[]> GetReportAsync(global::Syncfusion.Reports.Server.ReportSetting_P setting);

		global::System.Threading.Tasks.Task_P<global::Syncfusion.Reports.Server.SharedDatasetinfo_P> GetSharedDataSetAsync(
					global::Syncfusion.Reports.Server.ReportSetting_P setting, 
					string path);

		global::System.Threading.Tasks.Task_P<global::Syncfusion.Reports.Server.RecordInfo_P> GetDataAsync(
					global::Syncfusion.Reports.Server.ReportSetting_P Setting, 
					global::Syncfusion.Reports.Server.ReportDataInfo_P ReportInfo);

		global::System.Threading.Tasks.Task_P<global::Syncfusion.Reports.Server.ServiceDataSourceDefinition_P> GetSharedDataSourceDefinitionAsync(
					global::Syncfusion.Reports.Server.ReportSetting_P setting, 
					string dataSource);

		global::System.Threading.Tasks.Task_P<global::Syncfusion.Reports.Server.ExportData_P> ExportAsync(
					global::Syncfusion.Reports.Server.ReportSetting_P setting, 
					string exportType);

		global::System.Threading.Tasks.Task_P<bool> IsValidConnectionAsync(
					string connectionString, 
					string dataProvider);
	}
}

namespace System.Reflection
{
	[global::System.Runtime.InteropServices.McgRedirectedType("System.Reflection.DispatchProxy, System.Private.Interop, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f" +
		"7f11d50a3a")]
	public class DispatchProxy_P
	{
	}
}

namespace System.ServiceModel.Channels
{
	[global::System.Runtime.InteropServices.McgRedirectedType("System.ServiceModel.Channels.ServiceChannelProxy, System.Private.ServiceModel, Version=4.0.0.0, Culture=neutral," +
		" PublicKeyToken=b03f5f7f11d50a3a")]
	public class ServiceChannelProxy_P : global::System.Reflection.DispatchProxy
	{
		protected override object Invoke(
					global::System.Reflection.MethodInfo targetMethodInfo, 
					object[] args)
		{
			return null;
		}
	}
}

namespace System.Threading.Tasks
{
	[global::System.Runtime.InteropServices.McgRedirectedType("System.Threading.Tasks.Task`1, System.Private.Threading, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f" +
		"7f11d50a3a")]
	public class Task_P<TResult>
	{
	}
}

namespace Syncfusion.Reports.Server
{
	[global::System.Runtime.InteropServices.McgRedirectedType("Syncfusion.Reports.Server.ReportSetting, Syncfusion.SfReportViewer.UWP, Version=14.1460.0.41, Culture=neutral, P" +
		"ublicKeyToken=3d67ed1f87d44c89")]
	public class ReportSetting_P
	{
	}
}

namespace Syncfusion.Reports.Server
{
	[global::System.Runtime.InteropServices.McgRedirectedType("Syncfusion.Reports.Server.SharedDatasetinfo, Syncfusion.SfReportViewer.UWP, Version=14.1460.0.41, Culture=neutra" +
		"l, PublicKeyToken=3d67ed1f87d44c89")]
	public class SharedDatasetinfo_P
	{
	}
}

namespace Syncfusion.Reports.Server
{
	[global::System.Runtime.InteropServices.McgRedirectedType("Syncfusion.Reports.Server.RecordInfo, Syncfusion.SfReportViewer.UWP, Version=14.1460.0.41, Culture=neutral, Publ" +
		"icKeyToken=3d67ed1f87d44c89")]
	public class RecordInfo_P
	{
	}
}

namespace Syncfusion.Reports.Server
{
	[global::System.Runtime.InteropServices.McgRedirectedType("Syncfusion.Reports.Server.ReportDataInfo, Syncfusion.SfReportViewer.UWP, Version=14.1460.0.41, Culture=neutral, " +
		"PublicKeyToken=3d67ed1f87d44c89")]
	public class ReportDataInfo_P
	{
	}
}

namespace Syncfusion.Reports.Server
{
	[global::System.Runtime.InteropServices.McgRedirectedType("Syncfusion.Reports.Server.ServiceDataSourceDefinition, Syncfusion.SfReportViewer.UWP, Version=14.1460.0.41, Cult" +
		"ure=neutral, PublicKeyToken=3d67ed1f87d44c89")]
	public class ServiceDataSourceDefinition_P
	{
	}
}

namespace Syncfusion.Reports.Server
{
	[global::System.Runtime.InteropServices.McgRedirectedType("Syncfusion.Reports.Server.ExportData, Syncfusion.SfReportViewer.UWP, Version=14.1460.0.41, Culture=neutral, Publ" +
		"icKeyToken=3d67ed1f87d44c89")]
	public class ExportData_P
	{
	}
}
