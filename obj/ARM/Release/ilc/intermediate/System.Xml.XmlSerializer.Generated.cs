namespace System.Runtime.CompilerServices {
    internal class __BlockReflectionAttribute : Attribute { }
}

namespace Microsoft.Xml.Serialization.GeneratedAssembly {


    [System.Runtime.CompilerServices.__BlockReflection]
    public class XmlSerializationWriter1 : System.Xml.Serialization.XmlSerializationWriter {

        public void Write11_anyType(object o, string parentRuntimeNs = null, string parentCompileTimeNs = null) {
            string defaultNamespace = parentRuntimeNs ?? @"";
            WriteStartDocument();
            if (o == null) {
                WriteNullTagLiteral(@"anyType", defaultNamespace);
                return;
            }
            TopLevelElement();
            string namespace1 = ( parentCompileTimeNs == @"" && parentRuntimeNs != null ) ? parentRuntimeNs : @"";
            Write1_Object(@"anyType", namespace1, ((global::System.Object)o), true, false, namespace1, @"");
        }

        public void Write12_SharedDataSet(object o, string parentRuntimeNs = null, string parentCompileTimeNs = null) {
            string defaultNamespace = parentRuntimeNs ?? @"";
            WriteStartDocument();
            if (o == null) {
                WriteNullTagLiteral(@"SharedDataSet", defaultNamespace);
                return;
            }
            TopLevelElement();
            string namespace2 = ( parentCompileTimeNs == @"" && parentRuntimeNs != null ) ? parentRuntimeNs : @"";
            Write9_SharedDataSet(@"SharedDataSet", namespace2, ((global::Syncfusion.RDL.Data.SharedDataSet)o), true, false, namespace2, @"");
        }

        public void Write13_Item(object o, string parentRuntimeNs = null, string parentCompileTimeNs = null) {
            string defaultNamespace = parentRuntimeNs ?? @"";
            WriteStartDocument();
            if (o == null) {
                WriteNullTagLiteral(@"ArrayOfSerializableDictionaryOfStringListOfPositionInfo", defaultNamespace);
                return;
            }
            TopLevelElement();
            string namespace3 = ( parentCompileTimeNs == @"" && parentRuntimeNs != null ) ? parentRuntimeNs : @"";
            {
                global::Syncfusion.Olap.UWP.Engine.HeaderPositionsInfo a = (global::Syncfusion.Olap.UWP.Engine.HeaderPositionsInfo)((global::Syncfusion.Olap.UWP.Engine.HeaderPositionsInfo)o);
                if ((object)(a) == null) {
                    WriteNullTagLiteral(@"ArrayOfSerializableDictionaryOfStringListOfPositionInfo", defaultNamespace);
                }
                else {
                    WriteStartElement(@"ArrayOfSerializableDictionaryOfStringListOfPositionInfo", namespace3, null, false);
                    for (int ia = 0; ia < ((System.Collections.ICollection)a).Count; ia++) {
                        string namespace4 = ( parentCompileTimeNs == @"" && parentRuntimeNs != null ) ? parentRuntimeNs : @"";
                        WriteSerializable((System.Xml.Serialization.IXmlSerializable)((global::Syncfusion.Olap.UWP.Common.SerializableDictionary<global::System.String,global::System.Collections.Generic.List<global::Syncfusion.Olap.UWP.Engine.PositionInfo>>)a[ia]), @"SerializableDictionaryOfStringListOfPositionInfo", namespace4, true, true);
                    }
                    WriteEndElement();
                }
            }
        }

        public void Write14_ArrayOfPositionInfo(object o, string parentRuntimeNs = null, string parentCompileTimeNs = null) {
            string defaultNamespace = parentRuntimeNs ?? @"";
            WriteStartDocument();
            if (o == null) {
                WriteNullTagLiteral(@"ArrayOfPositionInfo", defaultNamespace);
                return;
            }
            TopLevelElement();
            string namespace5 = ( parentCompileTimeNs == @"" && parentRuntimeNs != null ) ? parentRuntimeNs : @"";
            {
                global::System.Collections.Generic.List<global::Syncfusion.Olap.UWP.Engine.PositionInfo> a = (global::System.Collections.Generic.List<global::Syncfusion.Olap.UWP.Engine.PositionInfo>)((global::System.Collections.Generic.List<global::Syncfusion.Olap.UWP.Engine.PositionInfo>)o);
                if ((object)(a) == null) {
                    WriteNullTagLiteral(@"ArrayOfPositionInfo", defaultNamespace);
                }
                else {
                    WriteStartElement(@"ArrayOfPositionInfo", namespace5, null, false);
                    for (int ia = 0; ia < ((System.Collections.ICollection)a).Count; ia++) {
                        string namespace6 = ( parentCompileTimeNs == @"" && parentRuntimeNs != null ) ? parentRuntimeNs : @"";
                        Write10_PositionInfo(@"PositionInfo", namespace6, ((global::Syncfusion.Olap.UWP.Engine.PositionInfo)a[ia]), true, false, namespace6, @"");
                    }
                    WriteEndElement();
                }
            }
        }

        public void Write15_anyType(object o, string parentRuntimeNs = null, string parentCompileTimeNs = null) {
            string defaultNamespace = parentRuntimeNs ?? @"";
            WriteStartDocument();
            if (o == null) {
                WriteNullTagLiteral(@"anyType", defaultNamespace);
                return;
            }
            TopLevelElement();
            string namespace7 = ( parentCompileTimeNs == @"" && parentRuntimeNs != null ) ? parentRuntimeNs : @"";
            Write1_Object(@"anyType", namespace7, ((global::System.Object)o), true, false, namespace7, @"");
        }

        public void Write16_string(object o, string parentRuntimeNs = null, string parentCompileTimeNs = null) {
            string defaultNamespace = parentRuntimeNs ?? @"";
            WriteStartDocument();
            if (o == null) {
                WriteNullTagLiteral(@"string", defaultNamespace);
                return;
            }
            TopLevelElement();
            string namespace8 = ( parentCompileTimeNs == @"" && parentRuntimeNs != null ) ? parentRuntimeNs : @"";
            WriteNullableStringLiteral(@"string", namespace8, ((global::System.String)o));
        }

        public void Write17_int(object o, string parentRuntimeNs = null, string parentCompileTimeNs = null) {
            string defaultNamespace = parentRuntimeNs ?? @"";
            WriteStartDocument();
            if (o == null) {
                WriteEmptyTag(@"int", defaultNamespace);
                return;
            }
            string namespace9 = ( parentCompileTimeNs == @"" && parentRuntimeNs != null ) ? parentRuntimeNs : @"";
            WriteElementStringRaw(@"int", namespace9, System.Xml.XmlConvert.ToString((global::System.Int32)((global::System.Int32)o)));
        }

        void Write1_Object(string n, string ns, global::System.Object o, bool isNullable, bool needType, string parentRuntimeNs = null, string parentCompileTimeNs = null) {
            string defaultNamespace = parentRuntimeNs;
            if ((object)o == null) {
                if (isNullable) WriteNullTagLiteral(n, ns);
                return;
            }
            if (!needType) {
                System.Type t = o.GetType();
                if (t == typeof(global::System.Object)) {
                }
                else if (t == typeof(global::Syncfusion.Olap.UWP.Engine.PositionInfo)) {
                    Write10_PositionInfo(n, ns,(global::Syncfusion.Olap.UWP.Engine.PositionInfo)o, isNullable, true);
                    return;
                }
                else if (t == typeof(global::Syncfusion.RDL.Data.SharedDataSet)) {
                    Write9_SharedDataSet(n, ns,(global::Syncfusion.RDL.Data.SharedDataSet)o, isNullable, true);
                    return;
                }
                else if (t == typeof(global::Syncfusion.RDL.Data.DataSetShared)) {
                    Write8_DataSetShared(n, ns,(global::Syncfusion.RDL.Data.DataSetShared)o, isNullable, true);
                    return;
                }
                else if (t == typeof(global::Syncfusion.RDL.DOM.Field)) {
                    Write7_Field(n, ns,(global::Syncfusion.RDL.DOM.Field)o, isNullable, true);
                    return;
                }
                else if (t == typeof(global::Syncfusion.RDL.Data.SharedQuery)) {
                    Write6_SharedQuery(n, ns,(global::Syncfusion.RDL.Data.SharedQuery)o, isNullable, true);
                    return;
                }
                else if (t == typeof(global::Syncfusion.RDL.DOM.Size)) {
                    Write3_Size(n, ns,(global::Syncfusion.RDL.DOM.Size)o, isNullable, true);
                    return;
                }
                else if (t == typeof(global::Syncfusion.Olap.UWP.Reports.AxisPosition)) {
                    Writer.WriteStartElement(n, ns);
                    WriteXsiType(@"AxisPosition", @"");
                    Writer.WriteString(Write2_AxisPosition((global::Syncfusion.Olap.UWP.Reports.AxisPosition)o));
                    Writer.WriteEndElement();
                    return;
                }
                else if (t == typeof(global::Syncfusion.RDL.DOM.Calendar)) {
                    Writer.WriteStartElement(n, ns);
                    WriteXsiType(@"Calendar", @"");
                    Writer.WriteString(Write4_Calendar((global::Syncfusion.RDL.DOM.Calendar)o));
                    Writer.WriteEndElement();
                    return;
                }
                else if (t == typeof(global::Syncfusion.RDL.DOM.CommandType)) {
                    Writer.WriteStartElement(n, ns);
                    WriteXsiType(@"CommandType", @"");
                    Writer.WriteString(Write5_CommandType((global::Syncfusion.RDL.DOM.CommandType)o));
                    Writer.WriteEndElement();
                    return;
                }
                else if (t == typeof(global::Syncfusion.RDL.DOM.Fields)) {
                    Writer.WriteStartElement(n, ns);
                    WriteXsiType(@"ArrayOfField", @"");
                    {
                        global::Syncfusion.RDL.DOM.Fields a = (global::Syncfusion.RDL.DOM.Fields)o;
                        if (a != null) {
                            for (int ia = 0; ia < ((System.Collections.ICollection)a).Count; ia++) {
                                string namespace10 = ( parentCompileTimeNs == @"" && parentRuntimeNs != null ) ? parentRuntimeNs : @"";
                                Write7_Field(@"Field", namespace10, ((global::Syncfusion.RDL.DOM.Field)a[ia]), true, false, namespace10, @"");
                            }
                        }
                    }
                    Writer.WriteEndElement();
                    return;
                }
                else if (t == typeof(global::Syncfusion.Olap.UWP.Engine.HeaderPositionsInfo)) {
                    Writer.WriteStartElement(n, ns);
                    WriteXsiType(@"ArrayOfSerializableDictionaryOfStringListOfPositionInfo", @"");
                    {
                        global::Syncfusion.Olap.UWP.Engine.HeaderPositionsInfo a = (global::Syncfusion.Olap.UWP.Engine.HeaderPositionsInfo)o;
                        if (a != null) {
                            for (int ia = 0; ia < ((System.Collections.ICollection)a).Count; ia++) {
                                string namespace11 = ( parentCompileTimeNs == @"" && parentRuntimeNs != null ) ? parentRuntimeNs : @"";
                                WriteSerializable((System.Xml.Serialization.IXmlSerializable)((global::Syncfusion.Olap.UWP.Common.SerializableDictionary<global::System.String,global::System.Collections.Generic.List<global::Syncfusion.Olap.UWP.Engine.PositionInfo>>)a[ia]), @"SerializableDictionaryOfStringListOfPositionInfo", namespace11, true, true);
                            }
                        }
                    }
                    Writer.WriteEndElement();
                    return;
                }
                else if (t == typeof(global::System.Collections.Generic.List<global::Syncfusion.Olap.UWP.Engine.PositionInfo>)) {
                    Writer.WriteStartElement(n, ns);
                    WriteXsiType(@"ArrayOfPositionInfo", @"");
                    {
                        global::System.Collections.Generic.List<global::Syncfusion.Olap.UWP.Engine.PositionInfo> a = (global::System.Collections.Generic.List<global::Syncfusion.Olap.UWP.Engine.PositionInfo>)o;
                        if (a != null) {
                            for (int ia = 0; ia < ((System.Collections.ICollection)a).Count; ia++) {
                                string namespace12 = ( parentCompileTimeNs == @"" && parentRuntimeNs != null ) ? parentRuntimeNs : @"";
                                Write10_PositionInfo(@"PositionInfo", namespace12, ((global::Syncfusion.Olap.UWP.Engine.PositionInfo)a[ia]), true, false, namespace12, @"");
                            }
                        }
                    }
                    Writer.WriteEndElement();
                    return;
                }
                else {
                    WriteTypedPrimitive(n, ns, o, true);
                    return;
                }
            }
            WriteStartElement(n, ns, o, false, null);
            WriteEndElement(o);
        }

        void Write10_PositionInfo(string n, string ns, global::Syncfusion.Olap.UWP.Engine.PositionInfo o, bool isNullable, bool needType, string parentRuntimeNs = null, string parentCompileTimeNs = null) {
            string defaultNamespace = parentRuntimeNs;
            if ((object)o == null) {
                if (isNullable) WriteNullTagLiteral(n, ns);
                return;
            }
            if (!needType) {
                System.Type t = o.GetType();
                if (t == typeof(global::Syncfusion.Olap.UWP.Engine.PositionInfo)) {
                }
                else {
                    throw CreateUnknownTypeException(o);
                }
            }
            WriteStartElement(n, ns, o, false, null);
            if (needType) WriteXsiType(@"PositionInfo", defaultNamespace);
            string namespace13 = ( parentCompileTimeNs == @"" && parentRuntimeNs != null ) ? parentRuntimeNs : @"";
            WriteElementString(@"UniqueName", namespace13, ((global::System.String)o.@UniqueName));
            string namespace14 = ( parentCompileTimeNs == @"" && parentRuntimeNs != null ) ? parentRuntimeNs : @"";
            WriteElementString(@"HierarchyUniqueName", namespace14, ((global::System.String)o.@HierarchyUniqueName));
            WriteEndElement(o);
        }

        void Write7_Field(string n, string ns, global::Syncfusion.RDL.DOM.Field o, bool isNullable, bool needType, string parentRuntimeNs = null, string parentCompileTimeNs = null) {
            string defaultNamespace = parentRuntimeNs;
            if ((object)o == null) {
                if (isNullable) WriteNullTagLiteral(n, ns);
                return;
            }
            if (!needType) {
                System.Type t = o.GetType();
                if (t == typeof(global::Syncfusion.RDL.DOM.Field)) {
                }
                else {
                    throw CreateUnknownTypeException(o);
                }
            }
            WriteStartElement(n, ns, o, false, null);
            if (needType) WriteXsiType(@"Field", defaultNamespace);
            WriteAttribute(@"Name", @"", ((global::System.String)o.@Name));
            string namespace15 = ( parentCompileTimeNs == @"" && parentRuntimeNs != null ) ? parentRuntimeNs : @"";
            WriteElementString(@"DataField", namespace15, ((global::System.String)o.@DataField));
            string namespace16 = ( parentCompileTimeNs == @"" && parentRuntimeNs != null ) ? parentRuntimeNs : @"";
            WriteElementString(@"Value", namespace16, ((global::System.String)o.@Value));
            string namespace17 = ( parentCompileTimeNs == @"http://schemas.microsoft.com/SQLServer/reporting/reportdesigner" && parentRuntimeNs != null ) ? parentRuntimeNs : @"http://schemas.microsoft.com/SQLServer/reporting/reportdesigner";
            WriteElementString(@"TypeName", namespace17, ((global::System.String)o.@TypeName));
            WriteEndElement(o);
        }

        string Write5_CommandType(global::Syncfusion.RDL.DOM.CommandType v) {
            string s = null;
            switch (v) {
                case global::Syncfusion.RDL.DOM.CommandType.@Text: s = @"Text"; break;
                case global::Syncfusion.RDL.DOM.CommandType.@StoredProcedure: s = @"StoredProcedure"; break;
                case global::Syncfusion.RDL.DOM.CommandType.@TableDirect: s = @"TableDirect"; break;
                default: throw CreateInvalidEnumValueException(((System.Int64)v).ToString(System.Globalization.CultureInfo.InvariantCulture), @"Syncfusion.RDL.DOM.CommandType");
            }
            return s;
        }

        string Write4_Calendar(global::Syncfusion.RDL.DOM.Calendar v) {
            string s = null;
            switch (v) {
                case global::Syncfusion.RDL.DOM.Calendar.@Default: s = @"Default"; break;
                case global::Syncfusion.RDL.DOM.Calendar.@Gregorian: s = @"Gregorian"; break;
                case global::Syncfusion.RDL.DOM.Calendar.@GregorianArabic: s = @"GregorianArabic"; break;
                case global::Syncfusion.RDL.DOM.Calendar.@GregorianMiddleEastFrench: s = @"GregorianMiddleEastFrench"; break;
                case global::Syncfusion.RDL.DOM.Calendar.@GregorianTransliteratedEnglish: s = @"GregorianTransliteratedEnglish"; break;
                case global::Syncfusion.RDL.DOM.Calendar.@GregorianTransliteratedFrench: s = @"GregorianTransliteratedFrench"; break;
                case global::Syncfusion.RDL.DOM.Calendar.@GregorianUSEnglish: s = @"GregorianUSEnglish"; break;
                case global::Syncfusion.RDL.DOM.Calendar.@Hebrew: s = @"Hebrew"; break;
                case global::Syncfusion.RDL.DOM.Calendar.@Hijri: s = @"Hijri"; break;
                case global::Syncfusion.RDL.DOM.Calendar.@Japanese: s = @"Japanese"; break;
                case global::Syncfusion.RDL.DOM.Calendar.@Korean: s = @"Korean"; break;
                case global::Syncfusion.RDL.DOM.Calendar.@Taiwan: s = @"Taiwan"; break;
                case global::Syncfusion.RDL.DOM.Calendar.@ThaiBuddhist: s = @"ThaiBuddhist"; break;
                default: throw CreateInvalidEnumValueException(((System.Int64)v).ToString(System.Globalization.CultureInfo.InvariantCulture), @"Syncfusion.RDL.DOM.Calendar");
            }
            return s;
        }

        string Write2_AxisPosition(global::Syncfusion.Olap.UWP.Reports.AxisPosition v) {
            string s = null;
            switch (v) {
                case global::Syncfusion.Olap.UWP.Reports.AxisPosition.@Categorical: s = @"Categorical"; break;
                case global::Syncfusion.Olap.UWP.Reports.AxisPosition.@Series: s = @"Series"; break;
                case global::Syncfusion.Olap.UWP.Reports.AxisPosition.@Slicer: s = @"Slicer"; break;
                default: throw CreateInvalidEnumValueException(((System.Int64)v).ToString(System.Globalization.CultureInfo.InvariantCulture), @"Syncfusion.Olap.UWP.Reports.AxisPosition");
            }
            return s;
        }

        void Write3_Size(string n, string ns, global::Syncfusion.RDL.DOM.Size o, bool isNullable, bool needType, string parentRuntimeNs = null, string parentCompileTimeNs = null) {
            string defaultNamespace = parentRuntimeNs;
            if ((object)o == null) {
                if (isNullable) WriteNullTagLiteral(n, ns);
                return;
            }
            if (!needType) {
                System.Type t = o.GetType();
                if (t == typeof(global::Syncfusion.RDL.DOM.Size)) {
                }
                else {
                    throw CreateUnknownTypeException(o);
                }
            }
            WriteStartElement(n, ns, o, false, null);
            if (needType) WriteXsiType(@"Size", defaultNamespace);
            if ((object)(o.@size) != null){
                WriteValue(((global::System.String)o.@size));
            }
            WriteEndElement(o);
        }

        void Write6_SharedQuery(string n, string ns, global::Syncfusion.RDL.Data.SharedQuery o, bool isNullable, bool needType, string parentRuntimeNs = null, string parentCompileTimeNs = null) {
            string defaultNamespace = parentRuntimeNs;
            if ((object)o == null) {
                if (isNullable) WriteNullTagLiteral(n, ns);
                return;
            }
            if (!needType) {
                System.Type t = o.GetType();
                if (t == typeof(global::Syncfusion.RDL.Data.SharedQuery)) {
                }
                else {
                    throw CreateUnknownTypeException(o);
                }
            }
            WriteStartElement(n, ns, o, false, null);
            if (needType) WriteXsiType(@"SharedQuery", defaultNamespace);
            string namespace18 = ( parentCompileTimeNs == @"" && parentRuntimeNs != null ) ? parentRuntimeNs : @"";
            WriteElementString(@"DataSourceReference", namespace18, ((global::System.String)o.@DataSourceReference));
            string namespace19 = ( parentCompileTimeNs == @"" && parentRuntimeNs != null ) ? parentRuntimeNs : @"";
            WriteElementString(@"CommandText", namespace19, ((global::System.String)o.@CommandText));
            string namespace20 = ( parentCompileTimeNs == @"" && parentRuntimeNs != null ) ? parentRuntimeNs : @"";
            WriteElementString(@"CommandType", namespace20, Write5_CommandType(((global::Syncfusion.RDL.DOM.CommandType)o.@CommandType)));
            WriteEndElement(o);
        }

        void Write8_DataSetShared(string n, string ns, global::Syncfusion.RDL.Data.DataSetShared o, bool isNullable, bool needType, string parentRuntimeNs = null, string parentCompileTimeNs = null) {
            string defaultNamespace = parentRuntimeNs;
            if ((object)o == null) {
                if (isNullable) WriteNullTagLiteral(n, ns);
                return;
            }
            if (!needType) {
                System.Type t = o.GetType();
                if (t == typeof(global::Syncfusion.RDL.Data.DataSetShared)) {
                }
                else {
                    throw CreateUnknownTypeException(o);
                }
            }
            WriteStartElement(n, ns, o, false, null);
            if (needType) WriteXsiType(@"DataSetShared", defaultNamespace);
            WriteAttribute(@"Name", @"", ((global::System.String)o.@Name));
            string namespace21 = ( parentCompileTimeNs == @"" && parentRuntimeNs != null ) ? parentRuntimeNs : @"";
            Write6_SharedQuery(@"Query", namespace21, ((global::Syncfusion.RDL.Data.SharedQuery)o.@Query), false, false, namespace21, @"");
            string namespace22 = ( parentCompileTimeNs == @"" && parentRuntimeNs != null ) ? parentRuntimeNs : @"";
            {
                global::Syncfusion.RDL.DOM.Fields a = (global::Syncfusion.RDL.DOM.Fields)((global::Syncfusion.RDL.DOM.Fields)o.@Fields);
                if (a != null){
                    WriteStartElement(@"Fields", namespace22, null, false);
                    for (int ia = 0; ia < ((System.Collections.ICollection)a).Count; ia++) {
                        string namespace23 = ( parentCompileTimeNs == @"" && parentRuntimeNs != null ) ? parentRuntimeNs : @"";
                        Write7_Field(@"Field", namespace23, ((global::Syncfusion.RDL.DOM.Field)a[ia]), true, false, namespace23, @"");
                    }
                    WriteEndElement();
                }
            }
            WriteEndElement(o);
        }

        void Write9_SharedDataSet(string n, string ns, global::Syncfusion.RDL.Data.SharedDataSet o, bool isNullable, bool needType, string parentRuntimeNs = null, string parentCompileTimeNs = null) {
            string defaultNamespace = parentRuntimeNs;
            if ((object)o == null) {
                if (isNullable) WriteNullTagLiteral(n, ns);
                return;
            }
            if (!needType) {
                System.Type t = o.GetType();
                if (t == typeof(global::Syncfusion.RDL.Data.SharedDataSet)) {
                }
                else {
                    throw CreateUnknownTypeException(o);
                }
            }
            WriteStartElement(n, ns, o, false, null);
            if (needType) WriteXsiType(@"SharedDataSet", defaultNamespace);
            string namespace24 = ( parentCompileTimeNs == @"" && parentRuntimeNs != null ) ? parentRuntimeNs : @"";
            Write8_DataSetShared(@"DataSet", namespace24, ((global::Syncfusion.RDL.Data.DataSetShared)o.@DataSet), false, false, namespace24, @"");
            WriteEndElement(o);
        }

        protected override void InitCallbacks() {
        }
    }

    [System.Runtime.CompilerServices.__BlockReflection]
    public class XmlSerializationReader1 : System.Xml.Serialization.XmlSerializationReader {

        public object Read11_anyType(string defaultNamespace = null) {
            object o = null;
            Reader.MoveToContent();
            if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                if (((object) Reader.LocalName == (object)id1_anyType && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                    o = Read1_Object(true, true, defaultNamespace);
                }
                else {
                    throw CreateUnknownNodeException();
                }
            }
            else {
                UnknownNode(null, defaultNamespace ?? @":anyType");
            }
            return (object)o;
        }

        public object Read12_SharedDataSet(string defaultNamespace = null) {
            object o = null;
            Reader.MoveToContent();
            if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                if (((object) Reader.LocalName == (object)id3_SharedDataSet && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                    o = Read9_SharedDataSet(true, true, defaultNamespace);
                }
                else {
                    throw CreateUnknownNodeException();
                }
            }
            else {
                UnknownNode(null, defaultNamespace ?? @":SharedDataSet");
            }
            return (object)o;
        }

        public object Read13_Item(string defaultNamespace = null) {
            object o = null;
            Reader.MoveToContent();
            if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                if (((object) Reader.LocalName == (object)id4_Item && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                    if (!ReadNull()) {
                        if ((object)(o) == null) o = new global::Syncfusion.Olap.UWP.Engine.HeaderPositionsInfo();
                        global::Syncfusion.Olap.UWP.Engine.HeaderPositionsInfo a_0_0 = (global::Syncfusion.Olap.UWP.Engine.HeaderPositionsInfo)o;
                        if ((Reader.IsEmptyElement)) {
                            Reader.Skip();
                        }
                        else {
                            Reader.ReadStartElement();
                            Reader.MoveToContent();
                            int whileIterations0 = 0;
                            int readerCount0 = ReaderCount;
                            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                                    if (((object) Reader.LocalName == (object)id5_Item && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                                        a_0_0.Add((global::Syncfusion.Olap.UWP.Common.SerializableDictionary<global::System.String,global::System.Collections.Generic.List<global::Syncfusion.Olap.UWP.Engine.PositionInfo>>)ReadSerializable(( System.Xml.Serialization.IXmlSerializable)new global::Syncfusion.Olap.UWP.Common.SerializableDictionary<global::System.String,global::System.Collections.Generic.List<global::Syncfusion.Olap.UWP.Engine.PositionInfo>>()));
                                    }
                                    else {
                                        UnknownNode(null, @":SerializableDictionaryOfStringListOfPositionInfo");
                                    }
                                }
                                else {
                                    UnknownNode(null, @":SerializableDictionaryOfStringListOfPositionInfo");
                                }
                                Reader.MoveToContent();
                                CheckReaderCount(ref whileIterations0, ref readerCount0);
                            }
                        ReadEndElement();
                        }
                    }
                    else {
                        if ((object)(o) == null) o = new global::Syncfusion.Olap.UWP.Engine.HeaderPositionsInfo();
                        global::Syncfusion.Olap.UWP.Engine.HeaderPositionsInfo a_0_0 = (global::Syncfusion.Olap.UWP.Engine.HeaderPositionsInfo)o;
                    }
                }
                else {
                    throw CreateUnknownNodeException();
                }
            }
            else {
                UnknownNode(null, defaultNamespace ?? @":ArrayOfSerializableDictionaryOfStringListOfPositionInfo");
            }
            return (object)o;
        }

        public object Read14_ArrayOfPositionInfo(string defaultNamespace = null) {
            object o = null;
            Reader.MoveToContent();
            if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                if (((object) Reader.LocalName == (object)id6_ArrayOfPositionInfo && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                    if (!ReadNull()) {
                        if ((object)(o) == null) o = new global::System.Collections.Generic.List<global::Syncfusion.Olap.UWP.Engine.PositionInfo>();
                        global::System.Collections.Generic.List<global::Syncfusion.Olap.UWP.Engine.PositionInfo> a_0_0 = (global::System.Collections.Generic.List<global::Syncfusion.Olap.UWP.Engine.PositionInfo>)o;
                        if ((Reader.IsEmptyElement)) {
                            Reader.Skip();
                        }
                        else {
                            Reader.ReadStartElement();
                            Reader.MoveToContent();
                            int whileIterations1 = 0;
                            int readerCount1 = ReaderCount;
                            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                                    if (((object) Reader.LocalName == (object)id7_PositionInfo && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                                        if ((object)(a_0_0) == null) Reader.Skip(); else a_0_0.Add(Read10_PositionInfo(true, true, defaultNamespace));
                                    }
                                    else {
                                        UnknownNode(null, @":PositionInfo");
                                    }
                                }
                                else {
                                    UnknownNode(null, @":PositionInfo");
                                }
                                Reader.MoveToContent();
                                CheckReaderCount(ref whileIterations1, ref readerCount1);
                            }
                        ReadEndElement();
                        }
                    }
                    else {
                        if ((object)(o) == null) o = new global::System.Collections.Generic.List<global::Syncfusion.Olap.UWP.Engine.PositionInfo>();
                        global::System.Collections.Generic.List<global::Syncfusion.Olap.UWP.Engine.PositionInfo> a_0_0 = (global::System.Collections.Generic.List<global::Syncfusion.Olap.UWP.Engine.PositionInfo>)o;
                    }
                }
                else {
                    throw CreateUnknownNodeException();
                }
            }
            else {
                UnknownNode(null, defaultNamespace ?? @":ArrayOfPositionInfo");
            }
            return (object)o;
        }

        public object Read15_anyType(string defaultNamespace = null) {
            object o = null;
            Reader.MoveToContent();
            if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                if (((object) Reader.LocalName == (object)id1_anyType && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                    o = Read1_Object(true, true, defaultNamespace);
                }
                else {
                    throw CreateUnknownNodeException();
                }
            }
            else {
                UnknownNode(null, defaultNamespace ?? @":anyType");
            }
            return (object)o;
        }

        public object Read16_string(string defaultNamespace = null) {
            object o = null;
            Reader.MoveToContent();
            if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                if (((object) Reader.LocalName == (object)id8_string && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                    if (ReadNull()) {
                        o = null;
                    }
                    else {
                        o = Reader.ReadElementContentAsString();
                    }
                }
                else {
                    throw CreateUnknownNodeException();
                }
            }
            else {
                UnknownNode(null, defaultNamespace ?? @":string");
            }
            return (object)o;
        }

        public object Read17_int(string defaultNamespace = null) {
            object o = null;
            Reader.MoveToContent();
            if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                if (((object) Reader.LocalName == (object)id9_int && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                    {
                        o = System.Xml.XmlConvert.ToInt32(Reader.ReadElementContentAsString());
                    }
                }
                else {
                    throw CreateUnknownNodeException();
                }
            }
            else {
                UnknownNode(null, defaultNamespace ?? @":int");
            }
            return (object)o;
        }

        global::System.Object Read1_Object(bool isNullable, bool checkType, string defaultNamespace = null) {
            System.Xml.XmlQualifiedName xsiType = checkType ? GetXsiType() : null;
            bool isNull = false;
            if (isNullable) isNull = ReadNull();
            if (checkType) {
                if (isNull) {
                    if (xsiType != null) return (global::System.Object)ReadTypedNull(xsiType);
                    else return null;
                }
                if (xsiType == null) {
                    return ReadTypedPrimitive(new System.Xml.XmlQualifiedName("anyType", "http://www.w3.org/2001/XMLSchema"));
                }
                else if (((object) ((System.Xml.XmlQualifiedName)xsiType).Name == (object)id7_PositionInfo && string.Equals( ((System.Xml.XmlQualifiedName)xsiType).Namespace, defaultNamespace ?? id2_Item)))
                    return Read10_PositionInfo(isNullable, false, defaultNamespace);
                else if (((object) ((System.Xml.XmlQualifiedName)xsiType).Name == (object)id3_SharedDataSet && string.Equals( ((System.Xml.XmlQualifiedName)xsiType).Namespace, defaultNamespace ?? id2_Item)))
                    return Read9_SharedDataSet(isNullable, false, defaultNamespace);
                else if (((object) ((System.Xml.XmlQualifiedName)xsiType).Name == (object)id10_DataSetShared && string.Equals( ((System.Xml.XmlQualifiedName)xsiType).Namespace, defaultNamespace ?? id2_Item)))
                    return Read8_DataSetShared(isNullable, false, defaultNamespace);
                else if (((object) ((System.Xml.XmlQualifiedName)xsiType).Name == (object)id11_Field && string.Equals( ((System.Xml.XmlQualifiedName)xsiType).Namespace, defaultNamespace ?? id2_Item)))
                    return Read7_Field(isNullable, false, defaultNamespace);
                else if (((object) ((System.Xml.XmlQualifiedName)xsiType).Name == (object)id12_SharedQuery && string.Equals( ((System.Xml.XmlQualifiedName)xsiType).Namespace, defaultNamespace ?? id2_Item)))
                    return Read6_SharedQuery(isNullable, false, defaultNamespace);
                else if (((object) ((System.Xml.XmlQualifiedName)xsiType).Name == (object)id13_Size && string.Equals( ((System.Xml.XmlQualifiedName)xsiType).Namespace, defaultNamespace ?? id2_Item)))
                    return Read3_Size(isNullable, false, defaultNamespace);
                else if (((object) ((System.Xml.XmlQualifiedName)xsiType).Name == (object)id14_AxisPosition && string.Equals( ((System.Xml.XmlQualifiedName)xsiType).Namespace, defaultNamespace ?? id2_Item))) {
                    Reader.ReadStartElement();
                    object e = Read2_AxisPosition(CollapseWhitespace(this.ReadString()));
                    ReadEndElement();
                    return e;
                }
                else if (((object) ((System.Xml.XmlQualifiedName)xsiType).Name == (object)id15_Calendar && string.Equals( ((System.Xml.XmlQualifiedName)xsiType).Namespace, defaultNamespace ?? id2_Item))) {
                    Reader.ReadStartElement();
                    object e = Read4_Calendar(CollapseWhitespace(this.ReadString()));
                    ReadEndElement();
                    return e;
                }
                else if (((object) ((System.Xml.XmlQualifiedName)xsiType).Name == (object)id16_CommandType && string.Equals( ((System.Xml.XmlQualifiedName)xsiType).Namespace, defaultNamespace ?? id2_Item))) {
                    Reader.ReadStartElement();
                    object e = Read5_CommandType(CollapseWhitespace(this.ReadString()));
                    ReadEndElement();
                    return e;
                }
                else if (((object) ((System.Xml.XmlQualifiedName)xsiType).Name == (object)id17_ArrayOfField && string.Equals( ((System.Xml.XmlQualifiedName)xsiType).Namespace, defaultNamespace ?? id2_Item))) {
                    global::Syncfusion.RDL.DOM.Fields a = null;
                    if (!ReadNull()) {
                        if ((object)(a) == null) a = new global::Syncfusion.RDL.DOM.Fields();
                        global::Syncfusion.RDL.DOM.Fields z_0_0 = (global::Syncfusion.RDL.DOM.Fields)a;
                        if ((Reader.IsEmptyElement)) {
                            Reader.Skip();
                        }
                        else {
                            Reader.ReadStartElement();
                            Reader.MoveToContent();
                            int whileIterations2 = 0;
                            int readerCount2 = ReaderCount;
                            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                                    if (((object) Reader.LocalName == (object)id11_Field && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                                        if ((object)(z_0_0) == null) Reader.Skip(); else z_0_0.Add(Read7_Field(true, true, defaultNamespace));
                                    }
                                    else {
                                        UnknownNode(null, @":Field");
                                    }
                                }
                                else {
                                    UnknownNode(null, @":Field");
                                }
                                Reader.MoveToContent();
                                CheckReaderCount(ref whileIterations2, ref readerCount2);
                            }
                        ReadEndElement();
                        }
                    }
                    return a;
                }
                else if (((object) ((System.Xml.XmlQualifiedName)xsiType).Name == (object)id4_Item && string.Equals( ((System.Xml.XmlQualifiedName)xsiType).Namespace, defaultNamespace ?? id2_Item))) {
                    global::Syncfusion.Olap.UWP.Engine.HeaderPositionsInfo a = null;
                    if (!ReadNull()) {
                        if ((object)(a) == null) a = new global::Syncfusion.Olap.UWP.Engine.HeaderPositionsInfo();
                        global::Syncfusion.Olap.UWP.Engine.HeaderPositionsInfo z_0_0 = (global::Syncfusion.Olap.UWP.Engine.HeaderPositionsInfo)a;
                        if ((Reader.IsEmptyElement)) {
                            Reader.Skip();
                        }
                        else {
                            Reader.ReadStartElement();
                            Reader.MoveToContent();
                            int whileIterations3 = 0;
                            int readerCount3 = ReaderCount;
                            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                                    if (((object) Reader.LocalName == (object)id5_Item && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                                        z_0_0.Add((global::Syncfusion.Olap.UWP.Common.SerializableDictionary<global::System.String,global::System.Collections.Generic.List<global::Syncfusion.Olap.UWP.Engine.PositionInfo>>)ReadSerializable(( System.Xml.Serialization.IXmlSerializable)new global::Syncfusion.Olap.UWP.Common.SerializableDictionary<global::System.String,global::System.Collections.Generic.List<global::Syncfusion.Olap.UWP.Engine.PositionInfo>>()));
                                    }
                                    else {
                                        UnknownNode(null, @":SerializableDictionaryOfStringListOfPositionInfo");
                                    }
                                }
                                else {
                                    UnknownNode(null, @":SerializableDictionaryOfStringListOfPositionInfo");
                                }
                                Reader.MoveToContent();
                                CheckReaderCount(ref whileIterations3, ref readerCount3);
                            }
                        ReadEndElement();
                        }
                    }
                    return a;
                }
                else if (((object) ((System.Xml.XmlQualifiedName)xsiType).Name == (object)id6_ArrayOfPositionInfo && string.Equals( ((System.Xml.XmlQualifiedName)xsiType).Namespace, defaultNamespace ?? id2_Item))) {
                    global::System.Collections.Generic.List<global::Syncfusion.Olap.UWP.Engine.PositionInfo> a = null;
                    if (!ReadNull()) {
                        if ((object)(a) == null) a = new global::System.Collections.Generic.List<global::Syncfusion.Olap.UWP.Engine.PositionInfo>();
                        global::System.Collections.Generic.List<global::Syncfusion.Olap.UWP.Engine.PositionInfo> z_0_0 = (global::System.Collections.Generic.List<global::Syncfusion.Olap.UWP.Engine.PositionInfo>)a;
                        if ((Reader.IsEmptyElement)) {
                            Reader.Skip();
                        }
                        else {
                            Reader.ReadStartElement();
                            Reader.MoveToContent();
                            int whileIterations4 = 0;
                            int readerCount4 = ReaderCount;
                            while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                                if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                                    if (((object) Reader.LocalName == (object)id7_PositionInfo && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                                        if ((object)(z_0_0) == null) Reader.Skip(); else z_0_0.Add(Read10_PositionInfo(true, true, defaultNamespace));
                                    }
                                    else {
                                        UnknownNode(null, @":PositionInfo");
                                    }
                                }
                                else {
                                    UnknownNode(null, @":PositionInfo");
                                }
                                Reader.MoveToContent();
                                CheckReaderCount(ref whileIterations4, ref readerCount4);
                            }
                        ReadEndElement();
                        }
                    }
                    return a;
                }
                else
                    return ReadTypedPrimitive((System.Xml.XmlQualifiedName)xsiType);
                }
                if (isNull) return null;
                global::System.Object o;
                o = new global::System.Object();
                bool[] paramsRead = new bool[0];
                while (Reader.MoveToNextAttribute()) {
                    if (!IsXmlnsAttribute(Reader.Name)) {
                        UnknownNode((object)o);
                    }
                }
                Reader.MoveToElement();
                if (Reader.IsEmptyElement) {
                    Reader.Skip();
                    return o;
                }
                Reader.ReadStartElement();
                Reader.MoveToContent();
                int whileIterations5 = 0;
                int readerCount5 = ReaderCount;
                while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                    if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                        UnknownNode((object)o, @"");
                    }
                    else {
                        UnknownNode((object)o, @"");
                    }
                    Reader.MoveToContent();
                    CheckReaderCount(ref whileIterations5, ref readerCount5);
                }
                ReadEndElement();
                return o;
            }

            global::Syncfusion.Olap.UWP.Engine.PositionInfo Read10_PositionInfo(bool isNullable, bool checkType, string defaultNamespace = null) {
                System.Xml.XmlQualifiedName xsiType = checkType ? GetXsiType() : null;
                bool isNull = false;
                if (isNullable) isNull = ReadNull();
                if (checkType) {
                if (xsiType == null || ((object) ((System.Xml.XmlQualifiedName)xsiType).Name == (object)id7_PositionInfo && string.Equals( ((System.Xml.XmlQualifiedName)xsiType).Namespace, defaultNamespace ?? id2_Item))) {
                }
                else
                    throw CreateUnknownTypeException((System.Xml.XmlQualifiedName)xsiType);
                }
                if (isNull) return null;
                global::Syncfusion.Olap.UWP.Engine.PositionInfo o;
                o = new global::Syncfusion.Olap.UWP.Engine.PositionInfo();
                bool[] paramsRead = new bool[2];
                while (Reader.MoveToNextAttribute()) {
                    if (!IsXmlnsAttribute(Reader.Name)) {
                        UnknownNode((object)o);
                    }
                }
                Reader.MoveToElement();
                if (Reader.IsEmptyElement) {
                    Reader.Skip();
                    return o;
                }
                Reader.ReadStartElement();
                Reader.MoveToContent();
                int whileIterations6 = 0;
                int readerCount6 = ReaderCount;
                while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                    if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                        if (!paramsRead[0] && ((object) Reader.LocalName == (object)id18_UniqueName && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                            {
                                o.@UniqueName = Reader.ReadElementContentAsString();
                            }
                            paramsRead[0] = true;
                        }
                        else if (!paramsRead[1] && ((object) Reader.LocalName == (object)id19_HierarchyUniqueName && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                            {
                                o.@HierarchyUniqueName = Reader.ReadElementContentAsString();
                            }
                            paramsRead[1] = true;
                        }
                        else {
                            UnknownNode((object)o, @":UniqueName, :HierarchyUniqueName");
                        }
                    }
                    else {
                        UnknownNode((object)o, @":UniqueName, :HierarchyUniqueName");
                    }
                    Reader.MoveToContent();
                    CheckReaderCount(ref whileIterations6, ref readerCount6);
                }
                ReadEndElement();
                return o;
            }

            global::Syncfusion.RDL.DOM.Field Read7_Field(bool isNullable, bool checkType, string defaultNamespace = null) {
                System.Xml.XmlQualifiedName xsiType = checkType ? GetXsiType() : null;
                bool isNull = false;
                if (isNullable) isNull = ReadNull();
                if (checkType) {
                if (xsiType == null || ((object) ((System.Xml.XmlQualifiedName)xsiType).Name == (object)id11_Field && string.Equals( ((System.Xml.XmlQualifiedName)xsiType).Namespace, defaultNamespace ?? id2_Item))) {
                }
                else
                    throw CreateUnknownTypeException((System.Xml.XmlQualifiedName)xsiType);
                }
                if (isNull) return null;
                global::Syncfusion.RDL.DOM.Field o;
                o = new global::Syncfusion.RDL.DOM.Field();
                bool[] paramsRead = new bool[4];
                while (Reader.MoveToNextAttribute()) {
                    if (!paramsRead[0] && ((object) Reader.LocalName == (object)id20_Name && string.Equals(Reader.NamespaceURI, id2_Item))) {
                        o.@Name = Reader.Value;
                        paramsRead[0] = true;
                    }
                    else if (!IsXmlnsAttribute(Reader.Name)) {
                        UnknownNode((object)o, @":Name");
                    }
                }
                Reader.MoveToElement();
                if (Reader.IsEmptyElement) {
                    Reader.Skip();
                    return o;
                }
                Reader.ReadStartElement();
                Reader.MoveToContent();
                int whileIterations7 = 0;
                int readerCount7 = ReaderCount;
                while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                    if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                        if (!paramsRead[1] && ((object) Reader.LocalName == (object)id21_DataField && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                            {
                                o.@DataField = Reader.ReadElementContentAsString();
                            }
                            paramsRead[1] = true;
                        }
                        else if (!paramsRead[2] && ((object) Reader.LocalName == (object)id22_Value && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                            {
                                o.@Value = Reader.ReadElementContentAsString();
                            }
                            paramsRead[2] = true;
                        }
                        else if (!paramsRead[3] && ((object) Reader.LocalName == (object)id23_TypeName && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id24_Item))) {
                            {
                                o.@TypeName = Reader.ReadElementContentAsString();
                            }
                            paramsRead[3] = true;
                        }
                        else {
                            UnknownNode((object)o, @":DataField, :Value, http://schemas.microsoft.com/SQLServer/reporting/reportdesigner:TypeName");
                        }
                    }
                    else {
                        UnknownNode((object)o, @":DataField, :Value, http://schemas.microsoft.com/SQLServer/reporting/reportdesigner:TypeName");
                    }
                    Reader.MoveToContent();
                    CheckReaderCount(ref whileIterations7, ref readerCount7);
                }
                ReadEndElement();
                return o;
            }

            global::Syncfusion.RDL.DOM.CommandType Read5_CommandType(string s) {
                switch (s) {
                    case @"Text": return global::Syncfusion.RDL.DOM.CommandType.@Text;
                    case @"StoredProcedure": return global::Syncfusion.RDL.DOM.CommandType.@StoredProcedure;
                    case @"TableDirect": return global::Syncfusion.RDL.DOM.CommandType.@TableDirect;
                    default: throw CreateUnknownConstantException(s, typeof(global::Syncfusion.RDL.DOM.CommandType));
                }
            }

            global::Syncfusion.RDL.DOM.Calendar Read4_Calendar(string s) {
                switch (s) {
                    case @"Default": return global::Syncfusion.RDL.DOM.Calendar.@Default;
                    case @"Gregorian": return global::Syncfusion.RDL.DOM.Calendar.@Gregorian;
                    case @"GregorianArabic": return global::Syncfusion.RDL.DOM.Calendar.@GregorianArabic;
                    case @"GregorianMiddleEastFrench": return global::Syncfusion.RDL.DOM.Calendar.@GregorianMiddleEastFrench;
                    case @"GregorianTransliteratedEnglish": return global::Syncfusion.RDL.DOM.Calendar.@GregorianTransliteratedEnglish;
                    case @"GregorianTransliteratedFrench": return global::Syncfusion.RDL.DOM.Calendar.@GregorianTransliteratedFrench;
                    case @"GregorianUSEnglish": return global::Syncfusion.RDL.DOM.Calendar.@GregorianUSEnglish;
                    case @"Hebrew": return global::Syncfusion.RDL.DOM.Calendar.@Hebrew;
                    case @"Hijri": return global::Syncfusion.RDL.DOM.Calendar.@Hijri;
                    case @"Japanese": return global::Syncfusion.RDL.DOM.Calendar.@Japanese;
                    case @"Korean": return global::Syncfusion.RDL.DOM.Calendar.@Korean;
                    case @"Taiwan": return global::Syncfusion.RDL.DOM.Calendar.@Taiwan;
                    case @"ThaiBuddhist": return global::Syncfusion.RDL.DOM.Calendar.@ThaiBuddhist;
                    default: throw CreateUnknownConstantException(s, typeof(global::Syncfusion.RDL.DOM.Calendar));
                }
            }

            global::Syncfusion.Olap.UWP.Reports.AxisPosition Read2_AxisPosition(string s) {
                switch (s) {
                    case @"Categorical": return global::Syncfusion.Olap.UWP.Reports.AxisPosition.@Categorical;
                    case @"Series": return global::Syncfusion.Olap.UWP.Reports.AxisPosition.@Series;
                    case @"Slicer": return global::Syncfusion.Olap.UWP.Reports.AxisPosition.@Slicer;
                    default: throw CreateUnknownConstantException(s, typeof(global::Syncfusion.Olap.UWP.Reports.AxisPosition));
                }
            }

            global::Syncfusion.RDL.DOM.Size Read3_Size(bool isNullable, bool checkType, string defaultNamespace = null) {
                System.Xml.XmlQualifiedName xsiType = checkType ? GetXsiType() : null;
                bool isNull = false;
                if (isNullable) isNull = ReadNull();
                if (checkType) {
                if (xsiType == null || ((object) ((System.Xml.XmlQualifiedName)xsiType).Name == (object)id13_Size && string.Equals( ((System.Xml.XmlQualifiedName)xsiType).Namespace, defaultNamespace ?? id2_Item))) {
                }
                else
                    throw CreateUnknownTypeException((System.Xml.XmlQualifiedName)xsiType);
                }
                if (isNull) return null;
                global::Syncfusion.RDL.DOM.Size o;
                o = new global::Syncfusion.RDL.DOM.Size();
                bool[] paramsRead = new bool[1];
                while (Reader.MoveToNextAttribute()) {
                    if (!IsXmlnsAttribute(Reader.Name)) {
                        UnknownNode((object)o);
                    }
                }
                Reader.MoveToElement();
                if (Reader.IsEmptyElement) {
                    Reader.Skip();
                    return o;
                }
                Reader.ReadStartElement();
                Reader.MoveToContent();
                int whileIterations8 = 0;
                int readerCount8 = ReaderCount;
                while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                    string tmp = null;
                    if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                        UnknownNode((object)o, @"");
                    }
                    else if (Reader.NodeType == System.Xml.XmlNodeType.Text || 
                    Reader.NodeType == System.Xml.XmlNodeType.CDATA || 
                    Reader.NodeType == System.Xml.XmlNodeType.Whitespace || 
                    Reader.NodeType == System.Xml.XmlNodeType.SignificantWhitespace) {
                        tmp = ReadString(tmp, false);
                        o.@size = tmp;
                    }
                    else {
                        UnknownNode((object)o, @"");
                    }
                    Reader.MoveToContent();
                    CheckReaderCount(ref whileIterations8, ref readerCount8);
                }
                ReadEndElement();
                return o;
            }

            global::Syncfusion.RDL.Data.SharedQuery Read6_SharedQuery(bool isNullable, bool checkType, string defaultNamespace = null) {
                System.Xml.XmlQualifiedName xsiType = checkType ? GetXsiType() : null;
                bool isNull = false;
                if (isNullable) isNull = ReadNull();
                if (checkType) {
                if (xsiType == null || ((object) ((System.Xml.XmlQualifiedName)xsiType).Name == (object)id12_SharedQuery && string.Equals( ((System.Xml.XmlQualifiedName)xsiType).Namespace, defaultNamespace ?? id2_Item))) {
                }
                else
                    throw CreateUnknownTypeException((System.Xml.XmlQualifiedName)xsiType);
                }
                if (isNull) return null;
                global::Syncfusion.RDL.Data.SharedQuery o;
                o = new global::Syncfusion.RDL.Data.SharedQuery();
                bool[] paramsRead = new bool[3];
                while (Reader.MoveToNextAttribute()) {
                    if (!IsXmlnsAttribute(Reader.Name)) {
                        UnknownNode((object)o);
                    }
                }
                Reader.MoveToElement();
                if (Reader.IsEmptyElement) {
                    Reader.Skip();
                    return o;
                }
                Reader.ReadStartElement();
                Reader.MoveToContent();
                int whileIterations9 = 0;
                int readerCount9 = ReaderCount;
                while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                    if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                        if (!paramsRead[0] && ((object) Reader.LocalName == (object)id25_DataSourceReference && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                            {
                                o.@DataSourceReference = Reader.ReadElementContentAsString();
                            }
                            paramsRead[0] = true;
                        }
                        else if (!paramsRead[1] && ((object) Reader.LocalName == (object)id26_CommandText && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                            {
                                o.@CommandText = Reader.ReadElementContentAsString();
                            }
                            paramsRead[1] = true;
                        }
                        else if (!paramsRead[2] && ((object) Reader.LocalName == (object)id16_CommandType && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                            {
                                o.@CommandType = Read5_CommandType(Reader.ReadElementContentAsString());
                            }
                            paramsRead[2] = true;
                        }
                        else {
                            UnknownNode((object)o, @":DataSourceReference, :CommandText, :CommandType");
                        }
                    }
                    else {
                        UnknownNode((object)o, @":DataSourceReference, :CommandText, :CommandType");
                    }
                    Reader.MoveToContent();
                    CheckReaderCount(ref whileIterations9, ref readerCount9);
                }
                ReadEndElement();
                return o;
            }

            global::Syncfusion.RDL.Data.DataSetShared Read8_DataSetShared(bool isNullable, bool checkType, string defaultNamespace = null) {
                System.Xml.XmlQualifiedName xsiType = checkType ? GetXsiType() : null;
                bool isNull = false;
                if (isNullable) isNull = ReadNull();
                if (checkType) {
                if (xsiType == null || ((object) ((System.Xml.XmlQualifiedName)xsiType).Name == (object)id10_DataSetShared && string.Equals( ((System.Xml.XmlQualifiedName)xsiType).Namespace, defaultNamespace ?? id2_Item))) {
                }
                else
                    throw CreateUnknownTypeException((System.Xml.XmlQualifiedName)xsiType);
                }
                if (isNull) return null;
                global::Syncfusion.RDL.Data.DataSetShared o;
                o = new global::Syncfusion.RDL.Data.DataSetShared();
                if ((object)(o.@Fields) == null) o.@Fields = new global::Syncfusion.RDL.DOM.Fields();
                global::Syncfusion.RDL.DOM.Fields a_2 = (global::Syncfusion.RDL.DOM.Fields)o.@Fields;
                bool[] paramsRead = new bool[3];
                while (Reader.MoveToNextAttribute()) {
                    if (!paramsRead[0] && ((object) Reader.LocalName == (object)id20_Name && string.Equals(Reader.NamespaceURI, id2_Item))) {
                        o.@Name = Reader.Value;
                        paramsRead[0] = true;
                    }
                    else if (!IsXmlnsAttribute(Reader.Name)) {
                        UnknownNode((object)o, @":Name");
                    }
                }
                Reader.MoveToElement();
                if (Reader.IsEmptyElement) {
                    Reader.Skip();
                    return o;
                }
                Reader.ReadStartElement();
                Reader.MoveToContent();
                int whileIterations10 = 0;
                int readerCount10 = ReaderCount;
                while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                    if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                        if (!paramsRead[1] && ((object) Reader.LocalName == (object)id27_Query && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                            o.@Query = Read6_SharedQuery(false, true, defaultNamespace);
                            paramsRead[1] = true;
                        }
                        else if (((object) Reader.LocalName == (object)id28_Fields && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                            if (!ReadNull()) {
                                if ((object)(o.@Fields) == null) o.@Fields = new global::Syncfusion.RDL.DOM.Fields();
                                global::Syncfusion.RDL.DOM.Fields a_2_0 = (global::Syncfusion.RDL.DOM.Fields)o.@Fields;
                                if ((Reader.IsEmptyElement)) {
                                    Reader.Skip();
                                }
                                else {
                                    Reader.ReadStartElement();
                                    Reader.MoveToContent();
                                    int whileIterations11 = 0;
                                    int readerCount11 = ReaderCount;
                                    while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                                        if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                                            if (((object) Reader.LocalName == (object)id11_Field && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                                                if ((object)(a_2_0) == null) Reader.Skip(); else a_2_0.Add(Read7_Field(true, true, defaultNamespace));
                                            }
                                            else {
                                                UnknownNode(null, @":Field");
                                            }
                                        }
                                        else {
                                            UnknownNode(null, @":Field");
                                        }
                                        Reader.MoveToContent();
                                        CheckReaderCount(ref whileIterations11, ref readerCount11);
                                    }
                                ReadEndElement();
                                }
                            }
                        }
                        else {
                            UnknownNode((object)o, @":Query, :Fields");
                        }
                    }
                    else {
                        UnknownNode((object)o, @":Query, :Fields");
                    }
                    Reader.MoveToContent();
                    CheckReaderCount(ref whileIterations10, ref readerCount10);
                }
                ReadEndElement();
                return o;
            }

            global::Syncfusion.RDL.Data.SharedDataSet Read9_SharedDataSet(bool isNullable, bool checkType, string defaultNamespace = null) {
                System.Xml.XmlQualifiedName xsiType = checkType ? GetXsiType() : null;
                bool isNull = false;
                if (isNullable) isNull = ReadNull();
                if (checkType) {
                if (xsiType == null || ((object) ((System.Xml.XmlQualifiedName)xsiType).Name == (object)id3_SharedDataSet && string.Equals( ((System.Xml.XmlQualifiedName)xsiType).Namespace, defaultNamespace ?? id2_Item))) {
                }
                else
                    throw CreateUnknownTypeException((System.Xml.XmlQualifiedName)xsiType);
                }
                if (isNull) return null;
                global::Syncfusion.RDL.Data.SharedDataSet o;
                o = new global::Syncfusion.RDL.Data.SharedDataSet();
                bool[] paramsRead = new bool[1];
                while (Reader.MoveToNextAttribute()) {
                    if (!IsXmlnsAttribute(Reader.Name)) {
                        UnknownNode((object)o);
                    }
                }
                Reader.MoveToElement();
                if (Reader.IsEmptyElement) {
                    Reader.Skip();
                    return o;
                }
                Reader.ReadStartElement();
                Reader.MoveToContent();
                int whileIterations12 = 0;
                int readerCount12 = ReaderCount;
                while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.NodeType != System.Xml.XmlNodeType.None) {
                    if (Reader.NodeType == System.Xml.XmlNodeType.Element) {
                        if (!paramsRead[0] && ((object) Reader.LocalName == (object)id29_DataSet && string.Equals(Reader.NamespaceURI, defaultNamespace ?? id2_Item))) {
                            o.@DataSet = Read8_DataSetShared(false, true, defaultNamespace);
                            paramsRead[0] = true;
                        }
                        else {
                            UnknownNode((object)o, @":DataSet");
                        }
                    }
                    else {
                        UnknownNode((object)o, @":DataSet");
                    }
                    Reader.MoveToContent();
                    CheckReaderCount(ref whileIterations12, ref readerCount12);
                }
                ReadEndElement();
                return o;
            }

            protected override void InitCallbacks() {
            }

            string id23_TypeName;
            string id22_Value;
            string id1_anyType;
            string id14_AxisPosition;
            string id24_Item;
            string id29_DataSet;
            string id8_string;
            string id6_ArrayOfPositionInfo;
            string id7_PositionInfo;
            string id5_Item;
            string id18_UniqueName;
            string id4_Item;
            string id27_Query;
            string id2_Item;
            string id10_DataSetShared;
            string id21_DataField;
            string id16_CommandType;
            string id11_Field;
            string id20_Name;
            string id12_SharedQuery;
            string id15_Calendar;
            string id13_Size;
            string id9_int;
            string id25_DataSourceReference;
            string id19_HierarchyUniqueName;
            string id26_CommandText;
            string id28_Fields;
            string id17_ArrayOfField;
            string id3_SharedDataSet;

            protected override void InitIDs() {
                id23_TypeName = Reader.NameTable.Add(@"TypeName");
                id22_Value = Reader.NameTable.Add(@"Value");
                id1_anyType = Reader.NameTable.Add(@"anyType");
                id14_AxisPosition = Reader.NameTable.Add(@"AxisPosition");
                id24_Item = Reader.NameTable.Add(@"http://schemas.microsoft.com/SQLServer/reporting/reportdesigner");
                id29_DataSet = Reader.NameTable.Add(@"DataSet");
                id8_string = Reader.NameTable.Add(@"string");
                id6_ArrayOfPositionInfo = Reader.NameTable.Add(@"ArrayOfPositionInfo");
                id7_PositionInfo = Reader.NameTable.Add(@"PositionInfo");
                id5_Item = Reader.NameTable.Add(@"SerializableDictionaryOfStringListOfPositionInfo");
                id18_UniqueName = Reader.NameTable.Add(@"UniqueName");
                id4_Item = Reader.NameTable.Add(@"ArrayOfSerializableDictionaryOfStringListOfPositionInfo");
                id27_Query = Reader.NameTable.Add(@"Query");
                id2_Item = Reader.NameTable.Add(@"");
                id10_DataSetShared = Reader.NameTable.Add(@"DataSetShared");
                id21_DataField = Reader.NameTable.Add(@"DataField");
                id16_CommandType = Reader.NameTable.Add(@"CommandType");
                id11_Field = Reader.NameTable.Add(@"Field");
                id20_Name = Reader.NameTable.Add(@"Name");
                id12_SharedQuery = Reader.NameTable.Add(@"SharedQuery");
                id15_Calendar = Reader.NameTable.Add(@"Calendar");
                id13_Size = Reader.NameTable.Add(@"Size");
                id9_int = Reader.NameTable.Add(@"int");
                id25_DataSourceReference = Reader.NameTable.Add(@"DataSourceReference");
                id19_HierarchyUniqueName = Reader.NameTable.Add(@"HierarchyUniqueName");
                id26_CommandText = Reader.NameTable.Add(@"CommandText");
                id28_Fields = Reader.NameTable.Add(@"Fields");
                id17_ArrayOfField = Reader.NameTable.Add(@"ArrayOfField");
                id3_SharedDataSet = Reader.NameTable.Add(@"SharedDataSet");
            }
        }

        [System.Runtime.CompilerServices.__BlockReflection]
        public abstract class XmlSerializer1 : System.Xml.Serialization.XmlSerializer {
            protected override System.Xml.Serialization.XmlSerializationReader CreateReader() {
                return new XmlSerializationReader1();
            }
            protected override System.Xml.Serialization.XmlSerializationWriter CreateWriter() {
                return new XmlSerializationWriter1();
            }
        }

        [System.Runtime.CompilerServices.__BlockReflection]
        public sealed class ObjectSerializer : XmlSerializer1 {

            public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
                return xmlReader.IsStartElement(@"anyType", this.DefaultNamespace ?? @"");
            }

            protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
                ((XmlSerializationWriter1)writer).Write11_anyType(objectToSerialize, this.DefaultNamespace, @"");
            }

            protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
                return ((XmlSerializationReader1)reader).Read11_anyType(this.DefaultNamespace);
            }
        }

        [System.Runtime.CompilerServices.__BlockReflection]
        public sealed class SharedDataSetSerializer : XmlSerializer1 {

            public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
                return xmlReader.IsStartElement(@"SharedDataSet", this.DefaultNamespace ?? @"");
            }

            protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
                ((XmlSerializationWriter1)writer).Write12_SharedDataSet(objectToSerialize, this.DefaultNamespace, @"");
            }

            protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
                return ((XmlSerializationReader1)reader).Read12_SharedDataSet(this.DefaultNamespace);
            }
        }

        [System.Runtime.CompilerServices.__BlockReflection]
        public sealed class HeaderPositionsInfoSerializer : XmlSerializer1 {

            public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
                return xmlReader.IsStartElement(@"ArrayOfSerializableDictionaryOfStringListOfPositionInfo", this.DefaultNamespace ?? @"");
            }

            protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
                ((XmlSerializationWriter1)writer).Write13_Item(objectToSerialize, this.DefaultNamespace, @"");
            }

            protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
                return ((XmlSerializationReader1)reader).Read13_Item(this.DefaultNamespace);
            }
        }

        [System.Runtime.CompilerServices.__BlockReflection]
        public sealed class ListOfPositionInfoSerializer : XmlSerializer1 {

            public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
                return xmlReader.IsStartElement(@"ArrayOfPositionInfo", this.DefaultNamespace ?? @"");
            }

            protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
                ((XmlSerializationWriter1)writer).Write14_ArrayOfPositionInfo(objectToSerialize, this.DefaultNamespace, @"");
            }

            protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
                return ((XmlSerializationReader1)reader).Read14_ArrayOfPositionInfo(this.DefaultNamespace);
            }
        }

        [System.Runtime.CompilerServices.__BlockReflection]
        public sealed class StringSerializer : XmlSerializer1 {

            public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
                return xmlReader.IsStartElement(@"string", this.DefaultNamespace ?? @"");
            }

            protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
                ((XmlSerializationWriter1)writer).Write16_string(objectToSerialize, this.DefaultNamespace, @"");
            }

            protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
                return ((XmlSerializationReader1)reader).Read16_string(this.DefaultNamespace);
            }
        }

        [System.Runtime.CompilerServices.__BlockReflection]
        public sealed class Int32Serializer : XmlSerializer1 {

            public override System.Boolean CanDeserialize(System.Xml.XmlReader xmlReader) {
                return xmlReader.IsStartElement(@"int", this.DefaultNamespace ?? @"");
            }

            protected override void Serialize(object objectToSerialize, System.Xml.Serialization.XmlSerializationWriter writer) {
                ((XmlSerializationWriter1)writer).Write17_int(objectToSerialize, this.DefaultNamespace, @"");
            }

            protected override object Deserialize(System.Xml.Serialization.XmlSerializationReader reader) {
                return ((XmlSerializationReader1)reader).Read17_int(this.DefaultNamespace);
            }
        }

        [System.Runtime.CompilerServices.__BlockReflection]
        public class XmlSerializerContract : global::System.Xml.Serialization.XmlSerializerImplementation {
            public override global::System.Xml.Serialization.XmlSerializationReader Reader { get { return new XmlSerializationReader1(); } }
            public override global::System.Xml.Serialization.XmlSerializationWriter Writer { get { return new XmlSerializationWriter1(); } }
            System.Collections.IDictionary readMethods = null;
            public override System.Collections.IDictionary ReadMethods {
                get {
                    if (readMethods == null) {
                        System.Collections.IDictionary _tmp = new System.Collections.Generic.Dictionary<string, string>();
                        _tmp[@"System.Object::"] = @"Read11_anyType";
                        _tmp[@"Syncfusion.RDL.Data.SharedDataSet::SharedDataSet:True:"] = @"Read12_SharedDataSet";
                        _tmp[@"Syncfusion.Olap.UWP.Engine.HeaderPositionsInfo::"] = @"Read13_Item";
                        _tmp[@"System.Collections.Generic.List`1[[Syncfusion.Olap.UWP.Engine.PositionInfo, Syncfusion.Olap.UWP, Version=14.1460.0.41, Culture=neutral, PublicKeyToken=3d67ed1f87d44c89]]::"] = @"Read14_ArrayOfPositionInfo";
                        _tmp[@"System.Object::"] = @"Read15_anyType";
                        _tmp[@"System.String::"] = @"Read16_string";
                        _tmp[@"System.Int32::"] = @"Read17_int";
                        if (readMethods == null) readMethods = _tmp;
                    }
                    return readMethods;
                }
            }
            System.Collections.IDictionary writeMethods = null;
            public override System.Collections.IDictionary WriteMethods {
                get {
                    if (writeMethods == null) {
                        System.Collections.IDictionary _tmp = new System.Collections.Generic.Dictionary<string, string>();
                        _tmp[@"System.Object::"] = @"Write11_anyType";
                        _tmp[@"Syncfusion.RDL.Data.SharedDataSet::SharedDataSet:True:"] = @"Write12_SharedDataSet";
                        _tmp[@"Syncfusion.Olap.UWP.Engine.HeaderPositionsInfo::"] = @"Write13_Item";
                        _tmp[@"System.Collections.Generic.List`1[[Syncfusion.Olap.UWP.Engine.PositionInfo, Syncfusion.Olap.UWP, Version=14.1460.0.41, Culture=neutral, PublicKeyToken=3d67ed1f87d44c89]]::"] = @"Write14_ArrayOfPositionInfo";
                        _tmp[@"System.Object::"] = @"Write15_anyType";
                        _tmp[@"System.String::"] = @"Write16_string";
                        _tmp[@"System.Int32::"] = @"Write17_int";
                        if (writeMethods == null) writeMethods = _tmp;
                    }
                    return writeMethods;
                }
            }
            System.Collections.IDictionary typedSerializers = null;
            public override System.Collections.IDictionary TypedSerializers {
                get {
                    if (typedSerializers == null) {
                        System.Collections.IDictionary _tmp = new System.Collections.Generic.Dictionary<string, System.Xml.Serialization.XmlSerializer>();
                        _tmp.Add(@"Syncfusion.RDL.Data.SharedDataSet::SharedDataSet:True:", new SharedDataSetSerializer());
                        _tmp.Add(@"System.String::", new StringSerializer());
                        _tmp.Add(@"System.Int32::", new Int32Serializer());
                        _tmp.Add(@"System.Object::", new ObjectSerializer());
                        _tmp.Add(@"Syncfusion.Olap.UWP.Engine.HeaderPositionsInfo::", new HeaderPositionsInfoSerializer());
                        _tmp.Add(@"System.Collections.Generic.List`1[[Syncfusion.Olap.UWP.Engine.PositionInfo, Syncfusion.Olap.UWP, Version=14.1460.0.41, Culture=neutral, PublicKeyToken=3d67ed1f87d44c89]]::", new ListOfPositionInfoSerializer());
                        if (typedSerializers == null) typedSerializers = _tmp;
                    }
                    return typedSerializers;
                }
            }
            public override System.Boolean CanSerialize(System.Type type) {
                if (type == typeof(global::System.Object)) return true;
                if (type == typeof(global::Syncfusion.RDL.Data.SharedDataSet)) return true;
                if (type == typeof(global::Syncfusion.Olap.UWP.Engine.HeaderPositionsInfo)) return true;
                if (type == typeof(global::System.Collections.Generic.List<global::Syncfusion.Olap.UWP.Engine.PositionInfo>)) return true;
                if (type == typeof(global::System.String)) return true;
                if (type == typeof(global::System.Int32)) return true;
                if (type == typeof(global::System.Reflection.TypeInfo)) return true;
                return false;
            }
            public override System.Xml.Serialization.XmlSerializer GetSerializer(System.Type type) {
                if (type == typeof(global::System.Object)) return new ObjectSerializer();
                if (type == typeof(global::Syncfusion.RDL.Data.SharedDataSet)) return new SharedDataSetSerializer();
                if (type == typeof(global::Syncfusion.Olap.UWP.Engine.HeaderPositionsInfo)) return new HeaderPositionsInfoSerializer();
                if (type == typeof(global::System.Collections.Generic.List<global::Syncfusion.Olap.UWP.Engine.PositionInfo>)) return new ListOfPositionInfoSerializer();
                if (type == typeof(global::System.Object)) return new ObjectSerializer();
                if (type == typeof(global::System.String)) return new StringSerializer();
                if (type == typeof(global::System.Int32)) return new Int32Serializer();
                return null;
            }
            public static global::System.Xml.Serialization.XmlSerializerImplementation GetXmlSerializerContract() { return new XmlSerializerContract(); }
        }

        [System.Runtime.CompilerServices.__BlockReflection]
        public static class ActivatorHelper {
            public static object CreateInstance(System.Type type) {
                System.Reflection.TypeInfo ti = System.Reflection.IntrospectionExtensions.GetTypeInfo(type);
                foreach (System.Reflection.ConstructorInfo ci in ti.DeclaredConstructors) {
                    if (!ci.IsStatic && ci.GetParameters().Length == 0) {
                        return ci.Invoke(null);
                    }
                }
                return System.Activator.CreateInstance(type);
            }
        }
    }
