﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Syncfusion.UI.Xaml.Gauges;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace charts
{
    public sealed partial class Radial2:Page
    {
        private DispatcherTimer timer = new DispatcherTimer();
        SfCircularGauge circularGauge = new SfCircularGauge();
        //private MainPageViewModel vm; 

        public int percentage_differnce = 50;

        public Radial2()
        {
            this.InitializeComponent();
            //vm = new MainPageViewModel(); 
            //this.DataContext = vm; 
            showRadial();
        }



        public void showRadial()
        {
            //timer.Interval = TimeSpan.FromSeconds(2); 
            timer.Start();
            int i = 0;

            timer.Tick += (object sender, object e) =>
            {

                if (i == percentage_differnce + 1)
                    timer.Stop();
                else
                {
                    gauge_pointer.Value = i;
                    i++;
                }
            };
        }

        private void back_btn_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(Radial));
        }
    }
}
