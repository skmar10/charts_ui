﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using CrossPieCharts.UWP.PieCharts;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI;


namespace charts
{
    public class DonutPieMainModel
    {
        public List<PieChartArgs> PieChartCollection { get; set; }

        public DonutPieMainModel()
        {
            PieChartCollection = new List<PieChartArgs>
            {
                new PieChartArgs
                {
                    Percentage =85,
                    ColorBrush = new SolidColorBrush(Colors.Green)
                },
                new PieChartArgs
                {
                    Percentage =15,
                    ColorBrush = new SolidColorBrush(Colors.Gray)
                },

            };
        }
    }

}
