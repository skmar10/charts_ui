﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using WinRTXamlToolkit.Controls.DataVisualization.Charting;

namespace charts
{
    public sealed partial class LineChrt :Page
    {

        public LineChrt()
        {
            this.InitializeComponent();
            this.Loaded += LineChart_Loaded;
        }

        public class TestProgress
        {
            public string Date { get; set; }
            public int Percentage { get; set; }
        }

        public class FinancialStuff
        {
            public string Name { get; set; }
            public int Amount { get; set; }
        }


        void LineChart_Loaded(object sender, RoutedEventArgs e)
        {
            LoadChartContents();
        }


        private void LoadChartContents()
        {

            List<TestProgress> progresslist = new List<TestProgress>();
            progresslist.Add(new TestProgress() { Date = "22/03/2016", Percentage = 96 });
            progresslist.Add(new TestProgress() { Date = "23/03/2016", Percentage = 86 });
            progresslist.Add(new TestProgress() { Date = "24/03/2016", Percentage = 76 });
            progresslist.Add(new TestProgress() { Date = "25/03/2016", Percentage = 66 });
            progresslist.Add(new TestProgress() { Date = "26/03/2016", Percentage = 56 });

            Random rand = new Random();
            List<FinancialStuff> financialStuffList = new List<FinancialStuff>();
            financialStuffList.Add(new FinancialStuff() { Name = "22/03/2016", Amount = rand.Next(0, 100) });
            financialStuffList.Add(new FinancialStuff() { Name = "23/03/2016", Amount = rand.Next(0, 100) });
            financialStuffList.Add(new FinancialStuff() { Name = "24/03/2016", Amount = rand.Next(0, 100) });
            financialStuffList.Add(new FinancialStuff() { Name = "25/03/2016", Amount = rand.Next(0, 100) });
            financialStuffList.Add(new FinancialStuff() { Name = "26/03/2016", Amount = rand.Next(0, 100) });
            financialStuffList.Add(new FinancialStuff() { Name = "27/03/2016", Amount = rand.Next(0, 100) });
            financialStuffList.Add(new FinancialStuff() { Name = "28/03/2016", Amount = rand.Next(0, 100) });
            financialStuffList.Add(new FinancialStuff() { Name = "29/03/2016", Amount = rand.Next(0, 100) });
            financialStuffList.Add(new FinancialStuff() { Name = "30/03/2016", Amount = rand.Next(0, 100) });
            financialStuffList.Add(new FinancialStuff() { Name = "31/03/2016", Amount = rand.Next(0, 100) });


            (LChrt.Series[0] as LineSeries).ItemsSource = financialStuffList;

        }

        private void click_previous(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(ColumnChrt));
        }

        private void click_Next(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(Radial));
        }
    }
}
